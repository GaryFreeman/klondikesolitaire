# Change Log
Все существенные изменения в этом проекте будут описаны в этом файле.
Этот проект следует [Семантическому Версионированию](http://semver.org/).

## [Не выпущено]

## [1.0.1] - 2016-05-29
### Исправлено
- Ссылки в ChangeLog'е.

## [1.0.0] - 2016-05-29
### Добавлено
- Базовая функциональность игры "Пасьянс Косынка".

[Не выпущено]: https://bitbucket.org/GaryFreeman/klondikesolitaire.git/branches/compare/develop..master
[1.0.1]: https://bitbucket.org/GaryFreeman/klondikesolitaire.git/branches/compare/v1.0.1..v1.0.0
[1.0.0]: https://bitbucket.org/GaryFreeman/klondikesolitaire.git/branches/compare/v1.0.0..3bd3744
