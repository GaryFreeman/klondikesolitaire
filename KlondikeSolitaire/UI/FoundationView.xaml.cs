﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KlondikeSolitaire.Data;
using KlondikeSolitaire.Misc;

namespace KlondikeSolitaire.UI
{
	public partial class FoundationView : UserControl
	{
		private static DependencyProperty FoundationProperty;

		static FoundationView()
		{
			var propertyMetadata = new PropertyMetadata(new Foundation(),
				new PropertyChangedCallback(OnFoundationChanged));
			FoundationProperty = DependencyProperty.Register("Foundation", typeof(Foundation), typeof(FoundationView),
				propertyMetadata);
		}

		public FoundationView()
		{
			InitializeComponent();

			Foundation = new Foundation();
		}

		/// <summary>
		/// Базовая стопка.
		/// </summary>
		public Foundation Foundation
		{
			get
			{
				return GetValue(FoundationProperty) as Foundation;
			}

			set
			{
				SetValue(FoundationProperty, value);
			}
		}

		protected override void OnMouseDown(MouseButtonEventArgs e)
		{
			base.OnMouseDown(e);

			if (Foundation.Count == 0) {
				// Если переносить нечего - ничего не делаем.

				return;
			}

			if (e.LeftButton == MouseButtonState.Pressed) {
				var data = new DataObject();
				data.SetData("foundation", Foundation);

				var desk = this.FindParent<DeskView>();
				var rootGrid = desk.FindName("RootGrid") as Grid;
				var canvas = rootGrid.FindName("Canvas") as Canvas;

				// Создаем временный вид, который будет следовать за мышкой.
				var card = new Card(Foundation.Peek());
				if (Foundation.Count > 1) {
					var first = true;
					Card secondCard = null;

					foreach (var card2 in Foundation) {
						if (first) {
							first = false;

							continue;
						}

						secondCard = card2;
						break;
					}

					Redraw(secondCard);
				} else {
					card.Visible = true;
					Foundation.Peek().Visible = false;
				}
				var movingCardView = new CardView();
				movingCardView.Card = card;
				canvas.Children.Add(movingCardView);

				// Задаем начальное положение временного вида.
				var point = TranslatePoint(new Point(0, 0), desk);
				Canvas.SetLeft(movingCardView, point.X);
				Canvas.SetTop(movingCardView, point.Y);

				// Получаем координату на карте, за которую ухватились.
				var anchorPosition = e.GetPosition(this);

				DragEventHandler dragOver = (s, e2) => {
					// Обновляем позицию временного вида.

					var mousePosition = e2.GetPosition(desk);
					Canvas.SetLeft(movingCardView, mousePosition.X - anchorPosition.X);
					Canvas.SetTop(movingCardView, mousePosition.Y - anchorPosition.Y);
				};
				DragDrop.AddDragOverHandler(desk, dragOver);

				// Инициализируем Drag-n-Drop.
				var effect = DragDrop.DoDragDrop(this, data, DragDropEffects.Move);

				// Удаляем временный вид и завершаем перенос.
				canvas.Children.Remove(movingCardView);
				if (Foundation.Count > 0) {
					Redraw();
					Foundation.Peek().Visible = true;
				}
				DragDrop.RemoveDragOverHandler(desk, dragOver);
			}
		}

		private static void OnFoundationChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var foundationView = sender as FoundationView;
			var foundation = e.NewValue as Foundation;

			var oldFoundation = e.OldValue as Foundation;
			if (oldFoundation != null) {
				oldFoundation.Invalidated -= foundationView.OnFoundationInvalidated;
			}

			foundation.Invalidated += foundationView.OnFoundationInvalidated;

			foundationView.Redraw();
		}

		private void OnFoundationInvalidated(object sender)
		{
			Redraw();
		}

		private void Redraw(Card card = null)
		{
			var cardView = FindName("CardView") as CardView;

			var notEmpty = Foundation.Count > 0;
			cardView.Visibility = (notEmpty || card != null) ? Visibility.Visible : Visibility.Collapsed;

			if (notEmpty || card != null) {
				if (card == null) {
					card = Foundation.Peek();
				}

				cardView.Card = card;
			}
		}
	}
}
