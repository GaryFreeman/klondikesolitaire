﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KlondikeSolitaire.Data;
using KlondikeSolitaire.Misc;

namespace KlondikeSolitaire.UI
{
	public partial class PileView : UserControl
	{
		private const int CARD_WIDTH = 90;
		private const int CARD_HEIGHT = 130;
		private const int CARD_OFFSET = 17;

		private static DependencyProperty PileProperty;

		static PileView()
		{
			var propertyMetadata = new PropertyMetadata(new Pile(), new PropertyChangedCallback(OnPileChanged));
			PileProperty = DependencyProperty.Register("Pile", typeof(Pile), typeof(PileView), propertyMetadata);
		}

		public PileView()
		{
			InitializeComponent();

			Pile = new Pile();
			Moving = false;
		}

		/// <summary>
		/// true, если вид представляет временную перемещаемую стопку.
		/// </summary>
		public bool Moving
		{
			get;
			private set;
		}

		/// <summary>
		/// Одна из 7 стопок в нижней части.
		/// </summary>
		public Pile Pile
		{
			get
			{
				return GetValue(PileProperty) as Pile;
			}

			set
			{
				SetValue(PileProperty, value);
			}
		}

		protected override void OnMouseDown(MouseButtonEventArgs e)
		{
			base.OnMouseDown(e);

			if (e.LeftButton == MouseButtonState.Pressed) {
				// Определяем количество переносимых карт.
				var position = e.GetPosition(Canvas);
				int cardIndex = Math.Min((int)position.Y / CARD_OFFSET, Pile.Count - 1);
				int number = Pile.Count - cardIndex;

				var correct = Pile.CheckSequence(number, CardValue.Any, SuitColor.Any);
				if (correct) {
					// Можем ухватиться за эту карту.

					var pile = Pile.BeginMovingCards(number);

					var data = new DataObject();
					data.SetData("pile", Pile);
					data.SetData("number", number);

					var desk = this.FindParent<DeskView>();
					var rootGrid = desk.FindName("RootGrid") as Grid;
					var canvas = rootGrid.FindName("Canvas") as Canvas;

					// Создаем временный вид, который будет следовать за мышкой.
					var movingPileView = new PileView();
					movingPileView.Moving = true;
					movingPileView.Pile = pile;
					canvas.Children.Add(movingPileView);

					// Задаем начальное положение временного вида.
					var point = TranslatePoint(new Point(0, 0), desk);
					point.Y += cardIndex * CARD_OFFSET;
					Canvas.SetLeft(movingPileView, point.X);
					Canvas.SetTop(movingPileView, point.Y);

					// Получаем координату на карте, за которую ухватились.
					var cardView = Canvas.Children[cardIndex + 1] as CardView;
					var anchorPosition = e.GetPosition(cardView);

					DragEventHandler dragOver = (s, e2) => {
						// Обновляем позицию временного вида.

						var mousePosition = e2.GetPosition(desk);
						Canvas.SetLeft(movingPileView, mousePosition.X - anchorPosition.X);
						Canvas.SetTop(movingPileView, mousePosition.Y - anchorPosition.Y);
					};
					DragDrop.AddDragOverHandler(desk, dragOver);

					// Инициализируем Drag-n-Drop.
					var effect = DragDrop.DoDragDrop(this, data, DragDropEffects.Move);

					// Удаляем временный вид и завершаем перенос.
					canvas.Children.Remove(movingPileView);
					Pile.EndMovingCards();
					DragDrop.RemoveDragOverHandler(desk, dragOver);
				}
			}
		}

		private static void OnPileChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var pileView = sender as PileView;
			var canvas = pileView.FindName("Canvas") as Canvas;
			var pile = e.NewValue as Pile;

			var oldPile = e.OldValue as Pile;
			if (oldPile != null) {
				oldPile.Invalidated -= pileView.OnPileInvalidated;

				// Удаляем все, кроме виджета места под стопку.
				canvas.Children.RemoveRange(1, canvas.Children.Count - 1);
			}

			pile.Invalidated += pileView.OnPileInvalidated;

			pileView.Redraw();
		}

		private void OnPileInvalidated(object sender)
		{
			var canvas = FindName("Canvas") as Canvas;

			// Удаляем все, кроме виджета места под стопку.
			canvas.Children.RemoveRange(1, canvas.Children.Count - 1);

			Redraw();
		}

		private void Redraw()
		{
			var totalHeight = CARD_HEIGHT + (Pile.Count - 1) * CARD_OFFSET;
			MinHeight = totalHeight;

			var canvas = FindName("Canvas") as Canvas;
			var i = 0;
			foreach (var card in Pile.Reversed()) {
				var cardView = new CardView();
				cardView.Card = card;

				canvas.Children.Add(cardView);
				Canvas.SetLeft(cardView, 0);
				Canvas.SetTop(cardView, i * CARD_OFFSET);

				++i;
			}
		}
	}
}
