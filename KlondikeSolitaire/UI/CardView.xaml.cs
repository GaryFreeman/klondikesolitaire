﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KlondikeSolitaire.Data;

namespace KlondikeSolitaire.UI
{
	/// <summary>
	/// Interaction logic for CardView.xaml
	/// </summary>
	public partial class CardView : UserControl
	{
		private static DependencyProperty CardProperty;

		static CardView()
		{
			var propertyMetadata = new PropertyMetadata(new Card(), new PropertyChangedCallback(OnCardChanged));
			CardProperty = DependencyProperty.Register("Card", typeof(Card), typeof(CardView), propertyMetadata);
		}

		public CardView()
		{
			InitializeComponent();

			Card = new Card {
				UpTurned = false
			};
		}

		/// <summary>
		/// Карта, которую представляет вид.
		/// </summary>
		public Card Card
		{
			get
			{
				return GetValue(CardProperty) as Card;
			}

			set
			{
				SetValue(CardProperty, value);
			}
		}

		private static void OnCardChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var cardView = sender as CardView;
			var card = e.NewValue as Card;

			var oldCard = e.OldValue as Card;
			if (oldCard != null) {
				oldCard.Invalidated -= cardView.OnCardInvalidated;
			}

			card.Invalidated += cardView.OnCardInvalidated;

			cardView.Redraw();
		}

		private void OnCardInvalidated(object sender)
		{
			Redraw();
		}

		private void Redraw()
		{
			ImageView.Visibility = Card.Visible ? Visibility.Visible : Visibility.Hidden;
			if (!Card.Visible) {
				return;
			}

			int cardX, cardY;
			if (Card.UpTurned) {
				cardX = (int)Card.Value - 1;
				cardY = (int)Card.Suit;
			} else {
				// Карта рубашкой вверх.

				cardX = 0;
				cardY = 4;
			}

			var rect = new Int32Rect(cardX * 90, cardY * 130, 90, 130);
			var bitmapSource =
					new BitmapImage(new Uri("pack://application:,,,/KlondikeSolitaire;component/UI/cards.png"));
			var source = new CroppedBitmap(bitmapSource, rect);

			ImageView.Source = source;
		}
	}
}
