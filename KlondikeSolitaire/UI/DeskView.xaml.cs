﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KlondikeSolitaire.Data;
using KlondikeSolitaire.Misc;

namespace KlondikeSolitaire.UI
{
	public partial class DeskView : UserControl
	{
		public DeskView()
		{
			InitializeComponent();

			var mainDeck = Deck.CreateStandart52Deck();
			mainDeck.Shuffle();

			for (var i = 0; i < 7; ++i) {
				var pile = Pile.CreatePileForColumn(mainDeck, i);

				var pileView = FindName("Pile" + (i + 1)) as PileView;
				pileView.Pile = pile;
			}

			var freeDeck = new FreeDeck(mainDeck);
			freeDeck.Mode = FreeDeck.ModeType.ByThree;

			FreeDeckView.FreeDeck = freeDeck;
			FreeDeckTakenView.FreeDeck = freeDeck;
		}

		/// <summary>
		/// Метод обработки Drop'а карт.
		/// </summary>
		private void OnPreviewDrop(object sender, DragEventArgs e)
		{
			e.Handled = true;

			var originalSource = MainDeskGrid.InputHitTest(e.GetPosition(this)) as UIElement;
			if (originalSource == null) {
				// Если перенесли "в пустоту" - ничего не делаем.

				return;
			}

			var pileView = originalSource.FindParent<PileView>();
			var foundationView = originalSource.FindParent<FoundationView>();

			var pile = e.Data.GetData("pile") as Pile;
			if (pile != null) {
				// Переносим С нижней стопки.

				var number = (int)e.Data.GetData("number");

				if (pileView != null) {
					// Переносим на нижнюю стопку.

					pileView.Pile.PutFromPile(pile, number);
				} else if (foundationView != null && number == 1) {
					// Переносим на базовую стопку.

					foundationView.Foundation.PutFromPile(pile);
				}

				return;
			}

			var foundation = e.Data.GetData("foundation") as Foundation;
			if (foundation != null) {
				// Переносим С базовой стопки.

				if (pileView != null) {
					// Переносим на нижнюю стопку.

					pileView.Pile.PutFromFoundation(foundation);
				} else if (foundationView != null) {
					// Переносим на базовую стопку.

					foundationView.Foundation.PutFromFoundation(foundation);
				}

				return;
			}

			var freeDeck = e.Data.GetData("freeDeck") as FreeDeck;
			if (freeDeck != null) {
				// Переносим СО свободной стопки.

				if (pileView != null) {
					// Переносим на нижнюю стопку.

					pileView.Pile.PutFromFreeDeck(freeDeck);
				} else if (foundationView != null) {
					// Переносим на базовую стопку.

					foundationView.Foundation.PutFromFreeDeck(freeDeck);
				}

				return;
			}
		}
	}
}
