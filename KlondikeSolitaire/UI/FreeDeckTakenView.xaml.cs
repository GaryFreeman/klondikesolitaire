﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KlondikeSolitaire.Data;
using KlondikeSolitaire.Misc;

namespace KlondikeSolitaire.UI
{
	public partial class FreeDeckTakenView : UserControl
	{
		private const int CARD_OFFSET = 45;

		private static DependencyProperty FreeDeckProperty;

		static FreeDeckTakenView()
		{
			var propertyMetadata = new PropertyMetadata(new FreeDeck(), new PropertyChangedCallback(OnFreeDeckChanged));
			FreeDeckProperty = DependencyProperty.Register("FreeDeck", typeof(FreeDeck), typeof(FreeDeckTakenView),
				propertyMetadata);
		}

		public FreeDeckTakenView()
		{
			InitializeComponent();

			FreeDeck = new FreeDeck();
		}

		/// <summary>
		/// Свободная стопка.
		/// </summary>
		public FreeDeck FreeDeck
		{
			get
			{
				return GetValue(FreeDeckProperty) as FreeDeck;
			}

			set
			{
				SetValue(FreeDeckProperty, value);
			}
		}

		protected override void OnMouseDown(MouseButtonEventArgs e)
		{
			base.OnMouseDown(e);

			if (FreeDeck.TakenDeck.Count == 0) {
				// Если со Свободной стопки ничего не сняли - ничего не делаем.

				return;
			}

			if (e.LeftButton == MouseButtonState.Pressed) {
				var data = new DataObject();
				data.SetData("freeDeck", FreeDeck);

				var desk = this.FindParent<DeskView>();
				var rootGrid = desk.FindName("RootGrid") as Grid;
				var canvas = rootGrid.FindName("Canvas") as Canvas;

				// Создаем временный вид, который будет следовать за мышкой.
				var card = new Card(FreeDeck.TakenDeck.Peek());
				card.Visible = true;
				FreeDeck.TakenDeck.Peek().Visible = false;

				var movingCardView = new CardView();
				movingCardView.Card = card;
				canvas.Children.Add(movingCardView);

				// Задаем начальное положение временного вида.
				var point = TranslatePoint(new Point(0, 0), desk);
				var offset = 0;
				if (FreeDeck.Mode == FreeDeck.ModeType.ByThree) {
					offset = Math.Min(FreeDeck.TakenDeck.Count - 1, 2) * CARD_OFFSET;
					point.X += offset;
				} else {
					if (FreeDeck.TakenDeck.Count > 1) {
						var first = true;
						Card secondCard = null;

						foreach (var card2 in FreeDeck.TakenDeck) {
							if (first) {
								first = false;

								continue;
							}

							secondCard = card2;
							break;
						}

						FreeDeck.TakenDeck.Peek().Visible = true;
						Redraw(secondCard);
					}
				}
				Canvas.SetLeft(movingCardView, point.X);
				Canvas.SetTop(movingCardView, point.Y);

				// Получаем координату на карте, за которую ухватились.
				var anchorPosition = e.GetPosition(this);

				DragEventHandler dragOver = (s, e2) => {
					// Обновляем позицию временного вида.

					var mousePosition = e2.GetPosition(desk);
					Canvas.SetLeft(movingCardView, mousePosition.X - anchorPosition.X + offset);
					Canvas.SetTop(movingCardView, mousePosition.Y - anchorPosition.Y);
				};
				DragDrop.AddDragOverHandler(desk, dragOver);

				// Инициализируем Drag-n-Drop.
				var effect = DragDrop.DoDragDrop(this, data, DragDropEffects.Move);

				// Удаляем временный вид и завершаем перенос.
				canvas.Children.Remove(movingCardView);
				if (FreeDeck.TakenDeck.Count > 0) {
					Redraw();
					FreeDeck.TakenDeck.Peek().Visible = true;
				}
				DragDrop.RemoveDragOverHandler(desk, dragOver);
			}
		}

		private static void OnFreeDeckChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var freeDeckTakenView = sender as FreeDeckTakenView;
			var freeDeck = e.NewValue as FreeDeck;

			var oldFreeDeck = e.OldValue as FreeDeck;
			if (oldFreeDeck != null) {
				oldFreeDeck.Invalidated -= freeDeckTakenView.OnFreeDeckInvalidated;
			}

			freeDeck.Invalidated += freeDeckTakenView.OnFreeDeckInvalidated;

			freeDeckTakenView.Redraw();
		}

		private void OnFreeDeckInvalidated(object sender)
		{
			Redraw();
		}

		private void Redraw(Card card = null)
		{
			Visibility[] visibilities;
			var cardViews = new CardView[] {
				Card1, Card2, Card3
			};

			if (FreeDeck.TakenDeck.Count > 0) {
				if (FreeDeck.Mode == FreeDeck.ModeType.ByOne) {
					visibilities = new Visibility[] {
						Visibility.Visible, Visibility.Collapsed, Visibility.Collapsed
					};

					if (card == null) {
						card = FreeDeck.TakenDeck.Peek();
					}
					Card1.Card = card;
				} else {
					visibilities = new Visibility[] {
						Visibility.Collapsed, Visibility.Collapsed, Visibility.Collapsed
					};

					var i = 0;
					foreach (var card2 in FreeDeck.TakenDeck) {
						if (i == 3) {
							break;
						}

						cardViews[Math.Min(FreeDeck.TakenDeck.Count, 3) - i - 1].Card = card2;
						visibilities[i] = Visibility.Visible;

						++i;
					}
				}
			} else {
				visibilities = new Visibility[] {
					Visibility.Collapsed, Visibility.Collapsed, Visibility.Collapsed
				};
			}

			for (var i = 0; i < cardViews.Length; ++i) {
				cardViews[i].Visibility = visibilities[i];
			}
		}
	}
}
