﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KlondikeSolitaire.Data;

namespace KlondikeSolitaire.UI
{
	public partial class FreeDeckView : UserControl
	{
		private static DependencyProperty FreeDeckProperty;

		static FreeDeckView()
		{
			var propertyMetadata = new PropertyMetadata(new FreeDeck(), new PropertyChangedCallback(OnFreeDeckChanged));
			FreeDeckProperty = DependencyProperty.Register("FreeDeck", typeof(FreeDeck), typeof(FreeDeckView),
				propertyMetadata);
		}

		public FreeDeckView()
		{
			InitializeComponent();

			CardView.Card = new Card {
				UpTurned = false
			};
			FreeDeck = new FreeDeck();
		}

		/// <summary>
		/// Свободная стопка.
		/// </summary>
		public FreeDeck FreeDeck
		{
			get
			{
				return GetValue(FreeDeckProperty) as FreeDeck;
			}

			set
			{
				SetValue(FreeDeckProperty, value);
			}
		}

		private static void OnFreeDeckChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var freeDeckView = sender as FreeDeckView;
			var freeDeck = e.NewValue as FreeDeck;

			var oldFreeDeck = e.OldValue as FreeDeck;
			if (oldFreeDeck != null) {
				oldFreeDeck.Invalidated -= freeDeckView.OnFreeDeckInvalidated;
			}

			freeDeck.Invalidated += freeDeckView.OnFreeDeckInvalidated;

			freeDeckView.Redraw();
		}

		private void OnFreeDeckClick(object sender, MouseButtonEventArgs e)
		{
			FreeDeck.Take();
		}

		private void OnFreeDeckInvalidated(object sender)
		{
			Redraw();
		}

		private void Redraw()
		{
			var cardView = FindName("CardView") as CardView;

			cardView.Visibility = (FreeDeck.Count > 0) ? Visibility.Visible : Visibility.Hidden;
		}
	}
}
