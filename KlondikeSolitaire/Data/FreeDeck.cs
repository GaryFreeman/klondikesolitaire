﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlondikeSolitaire.Data
{
	/// <summary>
	/// Свободная стопка, из которой можно сдавать карты по одной либо по три.
	/// </summary>
	public class FreeDeck : Deck
	{
		private Deck takenDeck = new Deck();

		public FreeDeck()
		{
			Mode = ModeType.ByOne;
		}

		public FreeDeck(ModeType mode)
		{
			Mode = mode;
		}

		public FreeDeck(FreeDeck original)
		{
			Mode = original.Mode;

			AddOneDeckToAnother(this, original);
			foreach (var card in this) {
				card.UpTurned = false;
			}
		}

		public FreeDeck(IEnumerable<Card> cards)
		{
			Mode = ModeType.ByOne;

			foreach (var card in cards.Reverse()) {
				Push(card);
			}
		}

		/// <summary>
		/// Режим сдачи карт (по одной или по три).
		/// </summary>
		public enum ModeType
		{
			/// <summary>
			/// Сдавать по одной карте.
			/// </summary>
			ByOne = 1,

			/// <summary>
			/// Сдавать по три карты.
			/// </summary>
			ByThree = 3
		}

		/// <see cref="ModeType"/>
		public ModeType Mode
		{
			get;
			set;
		}

		/// <summary>
		/// Открытая часть Свободной стопки.
		/// </summary>
		public Deck TakenDeck
		{
			get
			{
				return takenDeck;
			}

			private set
			{
				takenDeck = value;
			}
		}

		public static FreeDeck operator +(FreeDeck left, Deck right)
		{
			var freeDeck = new FreeDeck(left);

			AddOneDeckToAnother(freeDeck, right);
			foreach (var card in freeDeck) {
				card.UpTurned = false;
			}

			return freeDeck;
		}

		/// <summary>
		/// Возвращает одну или три карты из свободной стопки.
		///
		/// При достижении конца стопки, возвращает пустую колоду, затем начинает сначала.
		/// </summary>
		/// <returns>Колода.</returns>
		public Deck Take()
		{
			if (Count == 0) {
				var reversedTakenDeck = TakenDeck.Reversed();
				foreach (var card in reversedTakenDeck) {
					card.UpTurned = false;
				}

				AddOneDeckToAnother(this, reversedTakenDeck);
				TakenDeck.Clear();

				Invalidate();

				return new Deck();
			}

			var number = Math.Min((int)Mode, Count);
			var takenCards = PopMultipleCards(number);
			foreach (var card in takenCards) {
				card.UpTurned = true;
			}

			var reversedTakenCards = takenCards.Reversed();
			TakenDeck += reversedTakenCards;

			Invalidate();

			return reversedTakenCards;
		}
	}
}
