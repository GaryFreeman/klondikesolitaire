﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KlondikeSolitaire.Data
{
	/// <summary>
	/// Атрибут, задающий позицию на спрайте.
	/// </summary>
	public class SpriteRectAttribute : Attribute
	{
		public SpriteRectAttribute(int x, int y, int width, int height)
		{
			Rect = new Int32Rect(x, y, width, height);
		}

		public Int32Rect Rect
		{
			get;
			private set;
		}
	}
}
