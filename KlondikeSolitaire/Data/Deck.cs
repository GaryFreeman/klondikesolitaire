﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlondikeSolitaire.Data
{
	/// <summary>
	/// Колода карт.
	/// </summary>
	public class Deck : Stack<Card>
	{
		/// <summary>
		/// Создает пустую колоду.
		/// </summary>
		public Deck()
		{
		}

		/// <summary>
		/// Создает копию колоды.
		/// </summary>
		/// <param name="original">Оригинальная колода.</param>
		public Deck(Deck original)
		{
			foreach (var card in original.Reverse()) {
				Push(card);
			}
		}

		/// <summary>
		/// Создает колоду из набора карт.
		/// </summary>
		/// <param name="cards">Набор карт.</param>
		public Deck(IEnumerable<Card> cards)
		{
			foreach (var card in cards.Reverse()) {
				Push(card);
			}
		}

		public event InvalidateEventHandler Invalidated;

		/// <summary>
		/// Создает колоду определенной масти, от Туза до Короля.
		/// </summary>
		/// <param name="suit">Масть.</param>
		/// <returns>Колода.</returns>
		public static Deck CreateDeckOfSuit(Suit suit)
		{
			var Deck = new Deck();

			var values = Enum.GetValues(typeof(CardValue));
			Array.Reverse(values);

			foreach (CardValue cardValue in values) {
				if (cardValue == CardValue.Any) {
					continue;
				}

				Deck.Push(new Card {
					Suit = suit,
					Value = cardValue
				});
			}

			return Deck;
		}

		/// <summary>
		/// Создает стандартную 52 карточную колоду.
		/// </summary>
		/// <returns></returns>
		public static Deck CreateStandart52Deck()
		{
			var Deck = new Deck();

			foreach (Suit suit in Enum.GetValues(typeof(Suit))) {
				Deck += Deck.CreateDeckOfSuit(suit);
			}

			return Deck;
		}

		public static Deck operator +(Deck left, Deck right)
		{
			var deck = new Deck(left);

			AddOneDeckToAnother(deck, right);

			return deck;
		}

		/// <summary>
		/// Возвращает сверху колоды несколько карт в виде новой колоды, но не снимает их.
		/// </summary>
		/// <param name="number">Количество карт.</param>
		/// <returns>Новая колода.</returns>
		public Deck PeekMultipleCards(int number)
		{
			var reversedDeck = new Deck();

			var i = 0;
			foreach (var card in this) {
				if (i == number) {
					break;
				}

				reversedDeck.Push(card);

				++i;
			}

			// Перекладываем карты в обратном порядке, чтобы они лежали так, как будто их сняли за раз.
			var deck = new Deck();
			foreach (var card in reversedDeck) {
				deck.Push(card);
			}

			return deck;
		}

		/// <summary>
		/// Снимает сверху колоды несколько карт и возвращает их в виде новой колоды.
		/// </summary>
		/// <param name="number">Количество карт.</param>
		/// <returns>Новая колода.</returns>
		public virtual Deck PopMultipleCards(int number)
		{
			var reversedDeck = new Deck();

			for (var i = 0; i < number; ++i) {
				reversedDeck.Push(Pop());
			}

			// Перекладываем карты в обратном порядке, чтобы они лежали так, как будто их сняли за раз.
			var deck = new Deck();
			foreach (var card in reversedDeck) {
				deck.Push(card);
			}

			return deck;
		}

		/// <summary>
		/// Возвращает перевернутую колоду.
		/// </summary>
		/// <returns>Перевернутая колода.</returns>
		public Deck Reversed()
		{
			return new Deck(this.Reverse());
		}

		/// <summary>
		/// Перемешивает колоду.
		/// </summary>
		public void Shuffle()
		{
			var random = new Random();

			Card[] Deck = ToArray();
			Clear();

			// Тасование Фишера–Йетса.
			for (var i = Deck.Length - 1; i > 0; --i) {
				var j = random.Next(0, i + 1);

				var tmp = new Card(Deck[i]);
				Deck[i] = Deck[j];
				Deck[j] = tmp;
			}

			foreach (var card in Deck.Reverse()) {
				Push(card);
			}
		}

		public void Invalidate()
		{
			if (Invalidated != null) {
				Invalidated(this);
			}
		}

		/// <summary>
		/// Кладет одну стопку на другую.
		/// </summary>
		/// <param name="left">Стопка, на которую кладется другая.</param>
		/// <param name="right">Стопка, которую кладут.</param>
		protected static void AddOneDeckToAnother(Deck left, Deck right)
		{
			foreach (var card in right.Reverse()) {
				left.Push(card);
			}
		}
	}
}
