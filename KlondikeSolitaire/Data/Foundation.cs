﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlondikeSolitaire.Data
{
	/// <summary>
	/// Базовая стопка (или "дом").
	/// 
	/// Всего в игре их 4, в них складывают карты по мастям, от Туза до Короля. Как только все 4 стопки собраны, игра
	/// считается выиграной.
	/// </summary>
	public class Foundation : Deck
	{
		/// <summary>
		/// Масть этой базовой стопки.
		/// </summary>
		public Suit Suit
		{
			get;
			set;
		}

		/// <summary>
		/// Переносит карту с другой Базовой стопки.
		/// </summary>
		/// <param name="foundation">Базовая стопка.</param>
		/// <returns>true, если карта была перенесена.</returns>
		public bool PutFromFoundation(Foundation foundation)
		{
			var result = PutFromDeck(foundation);

			if (result) {
				Invalidate();
			}

			return result;
		}

		/// <summary>
		/// Переносит карту со Свободной стопки.
		/// </summary>
		/// <param name="freeDeck">Свободная стопка.</param>
		/// <returns>true, если карта была перенесена.</returns>
		public bool PutFromFreeDeck(FreeDeck freeDeck)
		{
			var result = PutFromDeck(freeDeck.TakenDeck);

			if (result) {
				Invalidate();
			}

			return result;
		}

		/// <summary>
		/// Переносит карту с одной из 7 колод в нижней части.
		/// </summary>
		/// <param name="pile">Одна из колода.</param>
		/// <returns>true, если карта была перенесена.</returns>
		public bool PutFromPile(Pile pile)
		{
			if (PutFromDeck(pile)) {
				if (pile.Count > 0) {
					// Переворачиваем последнюю карту в стопке, откуда была перенесена карта, вверх.

					pile.Peek().UpTurned = true;
				}

				Invalidate();

				return true;
			}

			return false;
		}

		/// <summary>
		/// Переносит карту из колоды.
		/// </summary>
		/// <param name="deck">Колода.</param>
		/// <returns>true, если карта была перенесена.</returns>
		private bool PutFromDeck(Deck deck)
		{
			if (deck.Count == 0) {
				return false;
			}

			if (Count == 0) {
				// Если в стопке еще нет карт, устанавливаем ее масть в масть переносимой карты.

				Suit = deck.Peek().Suit;
			} else if (deck.Peek().Suit != Suit) {
				// Если масть колоды не совпадает с мастью карты, ничего не делаем.

				return false;
			}

			int lastCardValue = 0;
			if (Count > 0) {
				lastCardValue = (int)Peek().Value;
			}

			if ((int)deck.Peek().Value - 1 != lastCardValue) {
				// Если не подходит предыдущая карта, ничего не делаем.

				return false;
			}

			Push(deck.Pop());
			deck.Invalidate();

			// Переворачиваем карту рубашкой вниз и делаем видимой.
			Peek().UpTurned = true;
			Peek().Visible = true;

			return true;
		}
	}
}
