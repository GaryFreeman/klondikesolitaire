﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlondikeSolitaire.Data
{
	/// <summary>
	/// Масти карт.
	/// </summary>
	public enum Suit
	{
		/// <summary>
		/// Крести.
		/// </summary>
		[Display(Name = "♣")]
		Club,

		/// <summary>
		/// Буби.
		/// </summary>
		[Display(Name = "♦")]
		Diamond,

		/// <summary>
		/// Черви.
		/// </summary>
		[Display(Name = "♥")]
		Heart,

		/// <summary>
		/// Пика.
		/// </summary>
		[Display(Name = "♠")]
		Spade
	}

	/// <summary>
	/// Цвета мастей.
	/// </summary>
	public enum SuitColor
	{
		Any,
		Black,
		Red
	}

	/// <summary>
	/// Достоинства карт.
	/// </summary>
	public enum CardValue
	{
		Any = 0,

		/// <summary>
		/// Туз.
		/// </summary>
		[Display(Name = "A")]
		Ace = 1,

		[Display(Name = "2")]
		Two,

		[Display(Name = "3")]
		Three,

		[Display(Name = "4")]
		Four,

		[Display(Name = "5")]
		Five,

		[Display(Name = "6")]
		Six,

		[Display(Name = "7")]
		Seven,

		[Display(Name = "8")]
		Eight,

		[Display(Name = "9")]
		Nine,

		[Display(Name = "10")]
		Ten,

		/// <summary>
		/// Валет.
		/// </summary>
		[Display(Name = "J")]
		Jack,

		/// <summary>
		/// Дама.
		/// </summary>
		[Display(Name = "Q")]
		Queen,

		/// <summary>
		/// Король.
		/// </summary>
		[Display(Name = "K")]
		King
	}

	/// <summary>
	/// Класс, представляющий одну карту.
	/// </summary>
	public class Card
	{
		private bool upTurned = true;
		private bool visible = true;

		public Card()
		{
			Visible = true;
		}

		public Card(Card original)
		{
			Suit = original.Suit;
			UpTurned = original.UpTurned;
			Value = original.Value;
			Visible = true;
		}

		public event InvalidateEventHandler Invalidated;

		/// <summary>
		/// Масть карты.
		/// </summary>
		public Suit Suit
		{
			get;
			set;
		}

		/// <summary>
		/// true, если карта лежит рубашкой вниз.
		/// </summary>
		public bool UpTurned
		{
			get
			{
				return upTurned;
			}

			set
			{
				upTurned = value;

				Invalidate();
			}
		}

		/// <summary>
		/// Достоинство карты.
		/// </summary>
		public CardValue Value
		{
			get;
			set;
		}

		/// <summary>
		/// Является ли карта видимой (может быть скрыта при перемещении).
		/// </summary>
		public bool Visible
		{
			get
			{
				return visible;
			}

			set
			{
				visible = value;

				Invalidate();
			}
		}

		/// <summary>
		/// Возвращает цвет масти.
		/// </summary>
		/// <param name="suit">Масть.</param>
		/// <returns>Цвет.</returns>
		public static SuitColor GetSuitColor(Suit suit)
		{
			if (suit == Suit.Club || suit == Suit.Spade) {
				return SuitColor.Black;
			} else {
				return SuitColor.Red;
			}
		}

		public override string ToString()
		{
			var cardSuit = GetEnumValueDisplayName(Suit);
			var cardValue = GetEnumValueDisplayName(Value);
			
			return cardSuit + cardValue;
		}

		/// <summary>
		/// Получает отображаемое имя значения enum'а.
		/// </summary>
		/// <param name="enumValue">Значение enum.</param>
		/// <returns>Отображаемое строковое имя.</returns>
		private string GetEnumValueDisplayName(Enum enumValue)
		{
			var type = enumValue.GetType();
			var valueMemberInfo = type.GetMember(Enum.GetName(type, enumValue))[0];
			var valueDisplayAttribute = (DisplayAttribute)valueMemberInfo.GetCustomAttributes(typeof(DisplayAttribute),
				false)[0];

			return valueDisplayAttribute.Name;
		}

		private void Invalidate()
		{
			if (Invalidated != null) {
				Invalidated(this);
			}
		}
	}
}
