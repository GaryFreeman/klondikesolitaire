﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlondikeSolitaire.Data
{
	/// <summary>
	/// Атрибут, задающий координату (x или y) на спрайте.
	/// </summary>
	public class SpriteCoordinateAttribute : Attribute
	{
		public SpriteCoordinateAttribute(int coordinate)
		{
			Coordinate = coordinate;
		}

		public int Coordinate
		{
			get;
			private set;
		}
	}
}
