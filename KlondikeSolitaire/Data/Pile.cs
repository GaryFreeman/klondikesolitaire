﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlondikeSolitaire.Data
{
	/// <summary>
	/// Класс семи колод, расположенных в нижней части.
	/// </summary>
	public class Pile : Deck
	{
		public Pile()
		{
		}

		public Pile(Pile original)
		{
			AddOneDeckToAnother(this, original);
		}

		/// <summary>
		/// Количество карт рубашкой вниз.
		/// </summary>
		public int UpTurnedCount
		{
			get
			{
				int number = 0;
				foreach (var card in this) {
					if (card.UpTurned) {
						++number;
					} else {
						break;
					}
				}

				return number;
			}
		}

		/// <summary>
		/// Создает одну из 7 стопок снизу по индексу стопки, беря карты из определенной колоды.
		/// </summary>
		/// <param name="deck">Исходная колода.</param>
		/// <param name="index">Индекс стопки.</param>
		/// <returns>Одна из 7 стопок снизу.</returns>
		public static Pile CreatePileForColumn(Deck deck, int index)
		{
			var pile = new Pile();

			pile += deck.PopMultipleCards(index + 1);
			var first = true;
			foreach (var card in pile) {
				card.UpTurned = first;
				first = false;
			}

			return pile;
		}

		public static Pile operator +(Pile left, Deck right)
		{
			var pile = new Pile(left);

			AddOneDeckToAnother(pile, right);

			return pile;
		}

		/// <summary>
		/// Вызывается при начале перемещения карт. Скрывает переносимую часть стопки, возвращает новую стопку из карт,
		/// которые переносим.
		/// 
		/// Вызывается в паре с методом <see cref="EndMovingCards"/>.
		/// </summary>
		/// <param name="number">Количество переносимых карт.</param>
		/// <returns>Подстопка, которую переносим.</returns>
		public Pile BeginMovingCards(int number)
		{
			var pile = new Pile();

			foreach (var card in PeekMultipleCards(number).Reversed()) {
				var newCard = new Card(card);

				card.Visible = false;

				pile.Push(newCard);
			}

			return pile;
		}

		/// <summary>
		/// Заканчивает перемещение карт.
		/// 
		/// Вызывается в паре с методом <see cref="BeginMovingCards"/>.
		/// </summary>
		public void EndMovingCards()
		{
			foreach (var card in this) {
				card.Visible = true;
			}
		}

		/// <summary>
		/// Проверяет последовательность карт на правильность (от старшей к младшей и чередование цвета).
		/// </summary>
		/// <param name="number">Количество карт.</param>
		/// <param name="lastCardValue">Необходимая сверху карта.</param>
		/// <param name="lastCardColor">Необходимый цвет верхней карты.</param>
		/// <returns>true, если последовательность правильная.</returns>
		public bool CheckSequence(int number, CardValue lastCardValue, SuitColor lastCardColor)
		{
			int i = 0;
			Card lastCard = null;
			foreach (var card in this) {
				if (i == number) {
					break;
				}

				if (!card.UpTurned) {
					// Карта рубашкой вверх.

					return false;
				}

				if (lastCard != null) {
					if ((int)lastCard.Value + 1 != (int)card.Value || Card.GetSuitColor(lastCard.Suit) ==
						Card.GetSuitColor(card.Suit)) {
						// Если предыдущая карта не на один меньше или цвета не чередуются - последовательность неправильная.

						return false;
					}
				}

				if (i + 1 == number) {
					// Последня карта.

					if (lastCardValue != CardValue.Any && card.Value != lastCardValue || lastCardColor != SuitColor.Any &&
						Card.GetSuitColor(card.Suit) != lastCardColor) {
						return false;
					}
				}

				lastCard = card;
				++i;
			}

			return true;
		}

		public override Deck PopMultipleCards(int number)
		{
			var deck = base.PopMultipleCards(number);

			Invalidate();

			return deck;
		}

		/// <summary>
		/// Переносит карту с одной из 4 базовых стопок.
		/// </summary>
		/// <param name="foundation">Базовая стопка.</param>
		/// <returns>true, если карта была перенесена.</returns>
		public bool PutFromFoundation(Foundation foundation)
		{
			var result = PutFromDeck(foundation);
			if (result) {
				Invalidate();

				return true;
			}

			return false;
		}

		/// <summary>
		/// Переносит карту со Свободной стопки.
		/// </summary>
		/// <param name="freeDeck">Свободная стопка.</param>
		/// <returns>true, если карта была перенесена.</returns>
		public bool PutFromFreeDeck(FreeDeck freeDeck)
		{
			var result = PutFromDeck(freeDeck.TakenDeck);
			if (result) {
				Invalidate();
				freeDeck.Invalidate();

				return true;
			}

			return false;
		}

		/// <summary>
		/// Переносит карту или несколько карт из другой стопки снизу.
		/// </summary>
		/// <param name="pile">Одна из 7 стопок снизу.</param>
		/// <param name="number">Количество карт.</param>
		/// <returns></returns>
		public bool PutFromPile(Pile pile, int number)
		{
			var lastCardValueNumber = (Count == 0) ? -1 : (int)Peek().Value - 1;
			if (lastCardValueNumber == 0) {
				// На туз нельзя ничего положить.

				return false;
			}

			var lastCardValue = (Count == 0) ? CardValue.King : (CardValue)lastCardValueNumber;
			SuitColor lastCardColor = SuitColor.Any;
			if (Count != 0) {
				if (Card.GetSuitColor(Peek().Suit) == SuitColor.Black) {
					lastCardColor = SuitColor.Red;
				} else {
					lastCardColor = SuitColor.Black;
				}
			}

			if (pile == this || pile.UpTurnedCount < number || !pile.CheckSequence(number, lastCardValue,
				lastCardColor)) {
				// Если мы переносим из той же самой стопки, карт не хватает или последовательность неправильная,
				// ничего не делаем.

				return false;
			}

			var takenCards = pile.PopMultipleCards(number);
			foreach (var card in takenCards) {
				card.Visible = true;
			}
			AddOneDeckToAnother(this, takenCards);

			Invalidate();

			// Переворачиваем последнюю карту рубашкой вниз.
			if (pile.Count > 0) {
				pile.Peek().UpTurned = true;
			}

			return true;
		}

		/// <summary>
		/// Переносит карту из колоды.
		/// </summary>
		/// <param name="deck">Колода.</param>
		/// <returns>true, если карта была перенесена.</returns>
		private bool PutFromDeck(Deck deck)
		{
			if (deck.Count == 0 || Count == 0 && deck.Peek().Value != CardValue.King) {
				// Если в колоде еще нет карт, проверяем, что переносится Король.

				return false;
			} else if (Count != 0 && Card.GetSuitColor(deck.Peek().Suit) == Card.GetSuitColor(Peek().Suit)) {
				// Если масти последней карты в колоде и переносимой совпадают, ничего не делаем (они должны
				// чередоваться).

				return false;
			}

			int lastCardValue = 14;
			if (Count > 0) {
				lastCardValue = (int)Peek().Value;
			}

			if ((int)deck.Peek().Value + 1 != lastCardValue) {
				// Если не подходит предыдущая карта, ничего не делаем.

				return false;
			}

			Push(deck.Pop());
			Peek().Visible = true;
			deck.Invalidate();

			// Переворачиваем карту рубашкой вниз.
			Peek().UpTurned = true;

			Invalidate();

			return true;
		}
	}
}
