﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KlondikeSolitaire.Misc
{
	public static class Extensions
	{
		public static T FindParent<T>(this DependencyObject child, string name = null) where T : DependencyObject
		{
			DependencyObject parentObject = LogicalTreeHelper.GetParent(child);

			if (parentObject == null) {
				return null;
			}

			T parent = parentObject as T;
			if (parent != null && (name == null || (string)parent.GetValue(FrameworkElement.NameProperty) == name)) {
				return parent;
			} else {
				return FindParent<T>(parentObject);
			}
		}
	}
}
